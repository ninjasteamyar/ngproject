# Generated by Django 3.2.8 on 2021-12-13 11:58

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('garden', '0005_auto_20211129_2221'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkTime',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.TimeField(verbose_name='Начало')),
                ('end_time', models.TimeField(verbose_name='Конец')),
            ],
            options={
                'verbose_name': 'Промежуток',
                'verbose_name_plural': 'Промежутки',
            },
        ),
        migrations.AlterModelOptions(
            name='child',
            options={'ordering': ('lastname', 'firstname', 'patronymic'), 'verbose_name': 'Ребенок(-а)', 'verbose_name_plural': 'Дети'},
        ),
        migrations.AlterModelOptions(
            name='parent',
            options={'ordering': ('lastname', 'firstname', 'patronymic'), 'verbose_name': 'Родитель', 'verbose_name_plural': 'Родители'},
        ),
        migrations.RenameField(
            model_name='child',
            old_name='surname',
            new_name='lastname',
        ),
        migrations.RenameField(
            model_name='parent',
            old_name='surname',
            new_name='lastname',
        ),
        migrations.AlterField(
            model_name='parent',
            name='phone',
            field=models.CharField(max_length=16, validators=[django.core.validators.RegexValidator(regex='^\\+?1?\\d{8,15}$')], verbose_name='Телефон'),
        ),
        migrations.CreateModel(
            name='Shedule',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата')),
                ('child', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='garden.child', verbose_name='Ребенок')),
                ('worktime', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='garden.worktime', verbose_name='Промежуток')),
            ],
        ),
    ]
