from django.db import models
from django.core.validators import RegexValidator


class Parent(models.Model):
    """класс Parent"""
    firstname = models.CharField(max_length=255, verbose_name='Имя')
    lastname = models.CharField(max_length=255, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=255, verbose_name='Отчество', blank=True)
    slug = models.SlugField(max_length=255, verbose_name='Url', unique=True, default=None)
    phone = models.CharField(validators=[RegexValidator(regex=r"^\+?1?\d{8,15}$")], max_length=16,
                             verbose_name='Телефон')
    email = models.EmailField(verbose_name='Электронная почта', blank=True)
    comment = models.CharField(max_length=1000, verbose_name='Комментарий', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __str__(self):
        return f'{self.lastname} {self.firstname}'

    class Meta:
        verbose_name = 'Родитель'
        verbose_name_plural = 'Родители'
        ordering = ('lastname', 'firstname', 'patronymic')


class Child(models.Model):
    """класс Child"""
    firstname = models.CharField(max_length=255, verbose_name='Имя')
    lastname = models.CharField(max_length=255, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=255, verbose_name='Отчество')
    date_of_birth = models.DateField(verbose_name='Дата рождения')
    slug = models.SlugField(max_length=255, verbose_name='Url', unique=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', blank=True, verbose_name='Фото')
    comment = models.CharField(max_length=1000, verbose_name='Комментарий', blank=True)
    parent = models.ManyToManyField(Parent, verbose_name='Родитель')

    def __str__(self):
        return f'{self.lastname} {self.firstname}'

    @property
    def title(self):
        return f'{self.lastname} {self.firstname}  {self.patronymic}'

    class Meta:
        verbose_name = 'Ребенок(-а)'
        verbose_name_plural = 'Дети'
        ordering = ('lastname', 'firstname', 'patronymic')


class WorkTime(models.Model):
    """ Класс промежуток работы"""
    start_time = models.TimeField(verbose_name='Начало')
    end_time = models.TimeField(verbose_name='Конец')

    def __str__(self):
        return f'{self.start_time} - {self.end_time}'

    class Meta:
        verbose_name = 'Промежуток'
        verbose_name_plural = 'Промежутки'
        ordering = ['start_time']


class Shedule(models.Model):
    """класс рассписание"""
    date = models.DateField(verbose_name='Дата')
    worktime = models.ForeignKey(WorkTime, verbose_name='Промежуток', on_delete=models.SET_NULL, null=True)
    child = models.ForeignKey(Child, verbose_name='Ребенок', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'

