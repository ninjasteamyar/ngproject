from django.shortcuts import render
from django.views.generic import TemplateView

from garden.models import Child


def index(request):
    return render(request, 'garden/index.html')


def register(request):
    pass
    # if request.method == 'POST':
    #     form = UserRegisterForm(request.POST)
    #     if form.is_valid():
    #         user = form.save()
    #         login(request, user)
    #         messages.success(request, 'Вы успешно зарегистрировались')
    #         return redirect('home')
    #     else:
    #         messages.error(request, 'Ошибка регистрации')
    # else:
    #     form = UserRegisterForm()
    # return render(request, 'news/register.html', {"form": form})


def user_login(request):
    pass
    # if request.method == 'POST':
    #     form = UserLoginForm(data=request.POST)
    #     if form.is_valid():
    #         user = form.get_user()
    #         login(request, user)
    #         return redirect('home')
    # else:
    #     form = UserLoginForm()
    # return render(request, 'news/login.html', {"form": form})


def user_logout(request):
    # logout(request)
    # return redirect('login')
    pass


class SheduleView(TemplateView):
    template_name = 'garden/shedule.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Расписание'
        context['active_class_sh'] = 'uk-active'
        context['childs'] = Child.objects.all()
        return context


class ChildView(TemplateView):
    template_name = 'garden/child.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Дети'
        context['active_class_ch'] = 'uk-active'
        context['childs'] = Child.objects.all()
        return context
