from django.contrib import admin

# Register your models here.
from django.utils.safestring import mark_safe

from garden.models import *


@admin.register(Child)
class ChildAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('lastname', 'firstname', 'patronymic')}
    save_on_top = True
    list_display = ('id', 'get_title', 'date_of_birth', 'get_photo')
    list_display_links = ('id', 'get_title')
    search_fields = ('firstname', 'lastname', 'patronymic')
    fields = ('lastname', 'firstname', 'patronymic', 'date_of_birth', 'slug', 'comment', 'parent', 'photo')

    def get_title(self, obj):
        return f'{obj.lastname} {obj.firstname}  {obj.patronymic}'

    def get_photo(self, obj):
        if obj.photo:
            return mark_safe(f'<img src="{obj.photo.url}" width="50">')
        return '-'

    get_photo.short_description = 'ФОТО'
    get_title.short_description = 'ФИО'


@admin.register(Parent)
class ParentAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('lastname', 'firstname', 'patronymic')}
    save_on_top = True
    list_display = ('id', 'get_title', 'phone')
    list_display_links = ('id', 'get_title')
    readonly_fields = ('created_at',)
    fields = ('lastname', 'firstname', 'patronymic', 'slug', 'phone', 'email', 'comment', 'created_at')
    search_fields = ('firstname', 'lastname', 'patronymic')

    def get_title(self, obj):
        return f'{obj.lastname} {obj.firstname}  {obj.patronymic}'

    get_title.short_description = 'ФИО'
    # def get_photo(self, obj):
    #     if obj.photo:
    #         return mark_safe(f'<img src="{obj.photo.url}" width="50">')
    #     return '-'
    #
    # get_photo.short_description = 'ФОТО'


@admin.register(WorkTime)
class WorkTimeAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(Shedule)
class SheduleAdmin(admin.ModelAdmin):
    save_on_top = True


# admin.site.register(Parent, ParentAdmin)
# admin.site.register(Child, ChildAdmin)
# admin.site.register(WorkTime, WorkTimeAdmin)
# admin.site.register(Shedule, SheduleAdmin)

admin.site.site_title = 'Ниндзя в саду'
admin.site.site_header = 'Управление садом'